// This config is in the KDL format: https://kdl.dev
// "/-" comments out the following node.
// Check the wiki for a full description of the configuration:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Overview

/-debug {
    // preview-render "screencast"
    // preview-render "screen-capture"
    // dbus-interfaces-in-non-session-instances
    // wait-for-frame-completion-before-queueing
    // enable-overlay-planes
    // disable-cursor-plane
    // disable-direct-scanout
    // restrict-primary-scanout-to-matching-format
    // render-drm-device "/dev/dri/renderD129"
    // render-drm-device "/dev/dri/renderD128"
    // render-drm-device "/dev/dri/card1"
    // force-pipewire-invalid-modifier
    // emulate-zero-presentation-time
    // disable-transactions
    // disable-resize-throttling
    // keep-laptop-panel-on-when-lid-is-closed
    // disable-monitor-names
    // strict-new-window-focus-policy
}

// Input device configuration.
// Find the full list of options on the wiki:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Input
input {
  keyboard {
    repeat-delay 250
    repeat-rate 35
  }
  touchpad {
    natural-scroll
    tap
    scroll-factor 0.5
  }
  mouse {
    natural-scroll
  }
  trackpoint {
    natural-scroll
  }
  trackball {
    natural-scroll
  }

  focus-follows-mouse max-scroll-amount="40%"
}

cursor {
  xcursor-theme "Bibata-Modern-Ice"

  hide-when-typing
}

output "DP-2" {
  mode "2560x1440@239.970"
}

hotkey-overlay {
  skip-at-startup
}

// Settings that influence how windows are positioned and sized.
// Find more information on the wiki:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Layout
layout {
  gaps 4
  center-focused-column "never"

  focus-ring {
    off
  }

  border {
    width 4
    active-gradient from="#483d8b" to="#6f3d8b" angle=45 relative-to="workspace-view"
    inactive-gradient from="#585b70" to="#7f849c" angle=45 relative-to="workspace-view"
  }

  preset-column-widths {
    proportion 0.33333
    proportion 0.5
    proportion 0.66667
  }

  default-column-width {
    proportion 0.4
  }

  struts {
    left 32
    right 32
  }
}

// Environment variables. Mostly to tell apps that we're in wayland
environment {
  DISPLAY ":1"
  ELM_DISPLAY "wl"
  GDK_BACKEND "wayland,x11"
  MOZ_ENABLE_WAYLAND "1"
  QT_AUTO_SCREEN_SCALE_FACTOR "1"
  QT_QPA_PLATFORM "wayland-egl"
  QT_QPA_PLATFORMTHEME "gtk3"
  QT_WAYLAND_DISABLE_WINDOWDECORATION "1"
  SDL_VIDEODRIVER "wayland"
  CLUTTER_BACKEND "wayland"
}

// workspaces
workspace "scratchpad"
workspace "main"
workspace "games"

// Things to run on startup
// setup environment
spawn-at-startup "dbus-update-activation-environment" "DISPLAY" "XDG_DATA_DIRS" "XAUTHORITY" "QT_QPA_PLATFORMTHEME" "QT_QPA_PLATFORM_THEME" "XDG_SEAT" "XDG_SEAT_PATH" "XDG_SESSION_PATH" "XDG_VTNR" "XDG_SESSION_TYPE" "LANG"
spawn-at-startup "turnstile-update-runit-env"         "DISPLAY" "XDG_DATA_DIRS" "XAUTHORITY" "QT_QPA_PLATFORMTHEME" "QT_QPA_PLATFORM_THEME" "XDG_SEAT" "XDG_SEAT_PATH" "XDG_SESSION_PATH" "XDG_VTNR" "XDG_SESSION_TYPE" "LANG"
// Useful apps for a desktop environment
spawn-at-startup "way-edges"
spawn-at-startup "darkman" "run"
spawn-at-startup "swww-daemon"
spawn-at-startup "swaync"
spawn-at-startup "swayidle" "-w"
spawn-at-startup "swayosd-server"
spawn-at-startup "pkexec" "swayosd-libinput-backend"
spawn-at-startup "udiskie" "--no-menu-update-workaround"
// Apps I want to start
spawn-at-startup "/home/chris/.local/bin/bing-wallpaper"
spawn-at-startup "xwayland-satellite" ":1"
spawn-at-startup "/home/chris/.local/bin/unlock-keepassxc"
spawn-at-startup "geary" "-n"
spawn-at-startup "flatpak" "run" "dev.deedles.Trayscale"
spawn-at-startup "flatpak" "run" "com.mardojai.ForgeSparks"
// Niri default workspace, so scratchpad is hidden
spawn-at-startup "niri" "msg" "action" "focus-workspace" "2"

// Uncomment this line to ask the clients to omit their client-side decorations if possible.
// If the client will specifically ask for CSD, the request will be honored.
// Additionally, clients will be informed that they are tiled, removing some rounded corners.
prefer-no-csd

// You can change the path where screenshots are saved.
// A ~ at the front will be expanded to the home directory.
// The path is formatted with strftime(3) to give you the screenshot date and time.
// You can also set this to null to disable saving screenshots to disk.
screenshot-path "~/Pictures/Screenshots/%Y-%m-%d--%H-%M-%S.png"

// Animation settings.
// The wiki explains how to configure individual animations:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Animations
// animations { }

// Window rules let you adjust behavior for individual windows.
// Find more information on the wiki:
// https://github.com/YaLTeR/niri/wiki/Configuration:-Window-Rules
window-rule {
  geometry-corner-radius 8
  clip-to-geometry true
  draw-border-with-background false
}

// Bigger scratchpad apps
window-rule {
  match app-id="tidal-hifi"
  match app-id="org.keepassxc.KeePassXC"
  match app-id="dev.deedles.Trayscale"
  match app-id="com.mardojai.ForgeSparks"
  match app-id="geary"

  exclude title=r#"^KeePassXC - Browser Access Request$"#

  open-on-workspace "scratchpad"
}

// Scratchpad Sizing
window-rule {
  match app-id="tidal-hifi"
  match app-id="com.mardojai.ForgeSparks"
  match app-id="geary"

  default-column-width { proportion 0.75; }
}

window-rule {
  match app-id="com.mardojai.ForgeSparks"

  default-column-width { proportion 0.2; }
}



// Browsers and alacritty should be bigger 
window-rule {
  match app-id="Firefox"
  match app-id="zen"
  match app-id="Alacritty"
  default-column-width { proportion 0.5; }
}

// Apps that should go to games
window-rule {
  match app-id="steam"
  match app-id="page.kramo.Cartridges"
  match app-id="itch"
  open-on-workspace "games"
}

// Gamescope opens into steam tv mode
window-rule {
  match app-id="gamescope"
  open-on-workspace "games"
  open-fullscreen true
  variable-refresh-rate true
}

binds {
  // Keys consist of modifiers separated by + signs, followed by an XKB key name
  // in the end. To find an XKB name for a particular key, you may use a program
  // like wev.
  //
  // "Mod" is a special modifier equal to Super when running on a TTY, and to Alt
  // when running as a winit window.
  //
  // Most actions that you can bind here can also be invoked programmatically with
  // `niri msg action do-something`.

  // Mod-Shift-/, which is usually the same as Mod-?,
  // shows a list of important hotkeys.
  Mod+Shift+Slash { show-hotkey-overlay; }

  // Suggested binds for running programs: terminal, app launcher, screen locker.
  Mod+T { spawn "alacritty"; }
  Mod+Space { spawn "anyrun"; }
  Mod+D { spawn "anyrun"; }
  Mod+N { spawn "nautilus"; }
  Super+Alt+N { spawn "swaync-client" "-t" "-sw"; }
  Super+Alt+L { spawn "swaylock"; }

  // Example volume keys mappings for PipeWire & WirePlumber.
  // The allow-when-locked=true property makes them work even when the session is locked.
  XF86AudioRaiseVolume  allow-when-locked=true { spawn "/home/chris/.local/bin/swayosd-bloop" "--output-volume" "raise"; }
  XF86AudioLowerVolume  allow-when-locked=true { spawn "/home/chris/.local/bin/swayosd-bloop" "--output-volume" "lower"; }
  XF86AudioMute         allow-when-locked=true { spawn "/home/chris/.local/bin/swayosd-bloop" "--output-volume" "mute-toggle"; }
  XF86AudioMicMute      allow-when-locked=true { spawn "/home/chris/.local/bin/swayosd-bloop" "--input-volume" "mute-toggle"; }

  // Brightness changing
  XF86MonBrightnessUp   allow-when-locked=true { spawn "/usr/bin/lightctl" "up"; }
  XF86MonBrightnessDown allow-when-locked=true { spawn "/usr/bin/lightctl" "down"; }

  // Quitting Apps
  Mod+Q { close-window; }
  Mod+Backspace { close-window; }

  // Navigating between windows and workspaces
  Mod+H { focus-column-left; }
  Mod+J { focus-window-or-workspace-down; }
  Mod+K { focus-window-or-workspace-up; }
  Mod+L { focus-column-right; }
  Mod+U { focus-workspace-down; }
  Mod+I { focus-workspace-up; }
  Mod+W { toggle-column-tabbed-display; }

  // Moving windows around
  Mod+Shift+H    { move-column-left; }
  Mod+Shift+J    { move-window-down-or-to-workspace-down; }
  Mod+Shift+K    { move-window-up-or-to-workspace-up; }
  Mod+Shift+L    { move-column-right; }
  Mod+Home       { focus-column-first; }
  Mod+End        { focus-column-last; }
  Mod+Shift+Home { move-column-to-first; }
  Mod+Shift+End  { move-column-to-last; }
  Mod+Shift+U    { move-column-to-workspace-down; }
  Mod+Shift+I    { move-column-to-workspace-up; }

  // You can bind mouse wheel scroll ticks using the following syntax.
  // These binds will change direction based on the natural-scroll setting.
  //
  // To avoid scrolling through workspaces really fast, you can use
  // the cooldown-ms property. The bind will be rate-limited to this value.
  // You can set a cooldown on any bind, but it's most useful for the wheel.
  Mod+WheelScrollDown      cooldown-ms=150 { focus-workspace-down; }
  Mod+WheelScrollUp        cooldown-ms=150 { focus-workspace-up; }
  Mod+Ctrl+WheelScrollDown cooldown-ms=150 { move-column-to-workspace-down; }
  Mod+Ctrl+WheelScrollUp   cooldown-ms=150 { move-column-to-workspace-up; }

  Mod+WheelScrollRight      { focus-column-right; }
  Mod+WheelScrollLeft       { focus-column-left; }
  Mod+Shift+WheelScrollRight { move-column-right; }
  Mod+Shift+WheelScrollLeft  { move-column-left; }

  // You can refer to workspaces by index. However, keep in mind that
  // niri is a dynamic workspace system, so these commands are kind of
  // "best effort". Trying to refer to a workspace index bigger than
  // the current workspace count will instead refer to the bottommost
  // (empty) workspace.
  //
  // For example, with 2 workspaces + 1 empty, indices 3, 4, 5 and so on
  // will all refer to the 3rd workspace.
  Mod+1 { focus-workspace 1; }
  Mod+2 { focus-workspace 2; }
  Mod+3 { focus-workspace 3; }
  Mod+4 { focus-workspace 4; }
  Mod+5 { focus-workspace 5; }
  Mod+6 { focus-workspace 6; }
  Mod+7 { focus-workspace 7; }
  Mod+8 { focus-workspace 8; }
  Mod+9 { focus-workspace 9; }

  Mod+Shift+1 { move-column-to-workspace 1; }
  Mod+Shift+2 { move-column-to-workspace 2; }
  Mod+Shift+3 { move-column-to-workspace 3; }
  Mod+Shift+4 { move-column-to-workspace 4; }
  Mod+Shift+5 { move-column-to-workspace 5; }
  Mod+Shift+6 { move-column-to-workspace 6; }
  Mod+Shift+7 { move-column-to-workspace 7; }
  Mod+Shift+8 { move-column-to-workspace 8; }
  Mod+Shift+9 { move-column-to-workspace 9; }

  // There are also commands that consume or expel a single window to the side.
  Mod+Comma { consume-or-expel-window-left; }
  Mod+Period { consume-or-expel-window-right; }

  // Window size (mostly width)
  Mod+R { switch-preset-column-width; }
  Mod+F { maximize-column; }
  Mod+Shift+F { fullscreen-window; }

  // Center current app
  Mod+C { center-column; }

  // Finer width adjustments.
  // This command can also:
  // * set width in pixels: "1000"
  // * adjust width in pixels: "-5" or "+5"
  // * set width as a percentage of screen width: "25%"
  // * adjust width as a percentage of screen width: "-10%" or "+10%"
  // Pixel sizes use logical, or scaled, pixels. I.e. on an output with scale 2.0,
  // set-column-width "100" will make the column occupy 200 physical screen pixels.
  Mod+Minus { set-column-width "-10%"; }
  Mod+Plus  { set-column-width "+10%"; }

  // Finer height adjustments when in column with other windows.
  Mod+Shift+Minus { set-window-height "-10%"; }
  Mod+Shift+Plus  { set-window-height "+10%"; }

  // Actions to switch layouts.
  // Note: if you uncomment these, make sure you do NOT have
  // a matching layout switch hotkey configured in xkb options above.
  // Having both at once on the same hotkey will break the switching,
  // since it will switch twice upon pressing the hotkey (once by xkb, once by niri).
  // Mod+Space       { switch-layout "next"; }
  // Mod+Shift+Space { switch-layout "prev"; }

  Mod+Shift+S { screenshot-screen; }
  Print       { screenshot; }
  Ctrl+Print  { screenshot-screen; }
  Alt+Print   { screenshot-window; }

  // The quit action will show a confirmation dialog to avoid accidental exits.
  Mod+Shift+E { quit; }

  // Powers off the monitors. To turn them back on, do any input like
  // moving the mouse or pressing any other key.
  Mod+Shift+P { power-off-monitors; }
}
