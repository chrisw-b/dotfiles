vim.api.nvim_create_autocmd("BufWritePre", {
  group = vim.api.nvim_create_augroup("format_on_save", { clear = true }),
  pattern = "*",
  desc = "Run LSP formatting on a file on save",
  callback = function(args)
    require("conform").format({ bufnr = args.buf })
  end,
})
