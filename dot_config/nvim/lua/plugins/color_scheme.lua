return {
  {
    "catppuccin",
    opts = {
      flavour = "frappe",
      show_end_of_buffer = false,
      styles = {
        comments = { "italic" }, -- Change the style of comments
      },
      integrations = {
        cmp = true,
        gitsigns = true,
        illuminate = true,
        indent_blankline = {
          enabled = false,
          scope_color = "sapphire",
          colored_indent_levels = false,
        },
        mason = true,
        native_lsp = { enabled = true },
        notify = true,
        nvimtree = true,
        neotree = true,
        symbols_outline = true,
        telescope = true,
      },
    },
  },
  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "catppuccin",
    },
  },
}
