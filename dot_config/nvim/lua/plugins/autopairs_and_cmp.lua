return {
    {
        "saghen/blink.cmp",
        version = 'v0.*',
        opts = {
            keymap = { preset = 'super-tab' },
            sources = { compat = { "cmp_jira" }, },
        },
    },
    "saghen/blink.compat",
    {
        "https://git.chriswb.dev/chrisw-b/cmp-jira",
        config = function()
            require("cmp_jira").setup({
                jira = { jql = 'assignee="%s"+AND+resolution=unresolved+order+by+updated+DESC', },
            })
        end,
    },
}
