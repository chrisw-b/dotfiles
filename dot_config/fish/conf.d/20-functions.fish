function free_port -d 'free port'
  if test $argv[1]
    command kill -9 (lsof -t -i :$argv[1])
  else
    echo 'please specify a port'
  end
end

function clone -d "clone something, cd into it. install it."
    git clone --depth=1 $argv[1]
    cd (basename $argv[1] | sed 's/.git$//')
    npm i
end

function clean_branches -d "trims deleted branches on all git repos in a directory"
    find . \( -path ./.Trashes -prune -o -path ./.Spotlight-V100 -prune -o -path ./.fseventsd -prune \) -o -type d -name .git -exec fish -c "cd \"{}\"/../ && pwd && git gone" \;
  end


function md --wraps mkdir -d "Create a directory and cd into it"
  mkdir $argv
  if test $status = 0
    switch $argv[(count $argv)]
      case '-*'
      case '*'
        cd $argv[(count $argv)]
        return
    end
  end
end


function sudo!!
    eval sudo $history[1]
end


function edit -d "Use the default visual editor"
  if set -q VISUAL
    eval $VISUAL $argv
  else
    eval $EDITOR $argv
  end
end

function sedit -d "Use the default shell editor"
  eval $EDITOR $argv
end
