# ====== Nice coreutil defaults =========
alias mv="mv -nv"
alias cp="cp -niv"
alias mkdir="mkdir -pv"
alias rm="rm --interactive=once -v"

# ====== Replace default programs =======
if command -q eza
    # replace ls with eza
    alias ls="eza --icons -F"
    alias la="ls --no-permissions --no-filesize -laho"
    alias lag="la --git"

    # replace tree with eza
    alias tree="eza --icons --git --tree --long -I \"node_modules\""
end

if command -q bat
    # replace cat with bat
    alias cat="bat"
    abbr --add --set-cursor --position anywhere -- -h '-h % | bat -plhelp'
    abbr --add --set-cursor --position anywhere -- --help '--help % | bat -plhelp'
end

# ====== void tools ======

if command -q xbps-uhelper
    alias installed_pkgs="xbps-query -m | xargs -n1 xbps-uhelper getpkgname"
end

# ====== For fun =======
alias please="sudo"
alias chmox="chmod +x"