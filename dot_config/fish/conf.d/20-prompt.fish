set -g async_prompt_functions _pure_prompt_git

# theming
set hydro_multiline true
set hydro_color_pwd $fish_color_param
set hydro_color_git $fish_color_autosuggestion
set hydro_color_prompt $fish_color_redirection