if status is-interactive
    and not set -q TMUX
    tmux attach -t {{ .machine_name }} || tmux new -s {{ .machine_name }} -e "DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS"
end

# pfetch config
set -x PF_INFO "title os kernel host shell editor pkgs uptime memory palette"
set -x PF_COL2 5
set -x PF_PACKAGE_MANAGERS on

function fish_greeting
    pfetch
end
fish_default_key_bindings

# set XDG locations if unset
set -q XDG_CONFIG_HOME || set -x XDG_CONFIG_HOME "{{.chezmoi.homeDir}}/.config"
set -q XDG_DATA_HOME || set -x XDG_DATA_HOME "{{.chezmoi.homeDir}}/.local/share"

{{- if eq .chezmoi.os "darwin" }}
set -x SSH_AUTH_SOCK "{{.chezmoi.homeDir}}/.strongbox/agent.sock"
{{- end }}

# setup custom bins
{{- if eq .chezmoi.os "darwin" }}
fish_add_path /opt/local/libexec/gnubin
fish_add_path /opt/local/bin
fish_add_path /opt/local/sbin
{{- end }}
fish_add_path {{.chezmoi.homeDir}}/.local/bin
fish_add_path /usr/local/bin

# Enable mise
if command -q mise
    mise activate fish | source
end

# Useful defaults
set -x LESSOPEN "| bat %s"
set -x GPG_TTY (tty)

# tmux config
set -x fish_tmux_config "$XDG_CONFIG_HOME/tmux/tmux.conf"
set -x TMUX_PLUGIN_MANAGER_PATH "$XDG_CONFIG_HOME/tmux/plugins"
set -x fish_tmux_unicode true

# zoxide config
set -x zoxide_cmd cd
set -x _ZO_ECHO 1

# tealdeer config
set -x TEALDEER_CONFIG_DIR "$XDG_CONFIG_HOME/tealdeer/config.toml"

{{- if eq .chezmoi.os "linux" }}
# Firefox config
set -x MOZ_DBUS_REMOTE 1
{{- end }}

# jira settings for nvim
set -x JIRA_USER_API_KEY "{{ .jira_user_api_key }}"
set -x JIRA_WORKSPACE_URL "{{ .jira_workspace }}"
set -x JIRA_USER_EMAIL "{{ .jira_email }}"

# editor config
if command -q nvim
    set -x EDITOR nvim
    set -x MANPAGER "nvim +Man! -"
else if command -q neovim
    set -x EDITOR neovim
    set -X MANPAGER "neovim +Man! -"
else
    set -x EDITOR vi
    set -x MANPAGER "vi +MANPAGER -"
end
set -x GIT_EDITOR "$EDITOR -c 'startinsert'"
set -x JJ_EDITOR "$EDITOR"
{{ if (and (eq .chezmoi.os "linux") (eq .remote_device "false")) }}
set -x VISUAL "subl -w"
{{ else if (and (eq .chezmoi.os "darwin") (eq .remote_device "false"))}}
set -x VISUAL "zed -w"
{{ end }}
set -x SUDO_EDITOR "$EDITOR"

# cargo config location
set -x CARGO_HOME "$XDG_CONFIG_HOME/cargo"
