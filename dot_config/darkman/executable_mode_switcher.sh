#!/bin/env bash

## INSTALLATION:
## install the darkman package
# mkdir -p $HOME/.config/darkman

## Put this script to $HOME/.config/darkman and make it executable:
# chmod +x $HOME/.config/darkman/mode_switcher.sh

## Some tuning the darkman for using only one script at one folder:
# sudo ln -s $HOME/.config/darkman /usr/share/dark-mode.d
# sudo ln -s $HOME/.config/darkman /usr/share/light-mode.d

case $(darkman get) in
light)
  # Set mode for GTK3 applications
  gsettings set org.gnome.desktop.interface gtk-theme Adwaita
  # Set mode for GTK4 applications
  niri msg action do-screen-transition && dconf write /org/gnome/desktop/interface/color-scheme '"prefer-light"'

  # Change swaync theme
  echo "@import \"style-light.css\";" > '/home/chris/.config/swaync/style.css'
  swaync-client --reload-css

  # Notification (for debug purposes)
  # notify-send "Switched" "to light mode!"
  ;;
dark)
  # Set mode for GTK3 applications. Install arc-gtk-theme, because there is no Adwaita-dark theme anymore
  gsettings set org.gnome.desktop.interface gtk-theme Arc-Dark
  # Set mode for GTK4 applications
  niri msg action do-screen-transition && dconf write /org/gnome/desktop/interface/color-scheme '"prefer-dark"'
  
  # Change swaync theme
  echo "@import \"style-dark.css\";" > '/home/chris/.config/swaync/style.css'
  swaync-client --reload-css

  # Notification (for debug purposes)
  # notify-send "Switched" "to dark mode!"
  ;;
esac

