-- hs.logger.defaultLogLevel = "info"
hs.loadSpoon("SpoonInstall")

local alt_cmd       = { "cmd", "alt" }
local shift_alt_cmd = { "shift", "cmd", "alt" }
local shift_hyper   = { "shift", "cmd", "alt", "ctrl" }

Install = spoon.SpoonInstall

Install.repos.PaperWM = {
    url = "https://github.com/mogenson/PaperWM.spoon",
    desc = "PaperWM.spoon repository",
    branch = "release",
}

Install:updateAllRepos()
Install:andUse("EmmyLua")
Install:andUse("PaperWM", { repo = "PaperWM", config = { screen_margin = 16, window_gap = 8 } })
Install:andUse("BingDaily", { config = { uhd_resolution = true, start = true } })

PaperWM = spoon.PaperWM

PaperWM:bindHotkeys({
    -- switch to a new focused window in tiled grid
    focus_left           = { alt_cmd, "h" },
    focus_right          = { alt_cmd, "l" },
    focus_up             = { alt_cmd, "k" },
    focus_down           = { alt_cmd, "j" },

    -- move windows around in tiled grid
    swap_left            = { shift_alt_cmd, "h" },
    swap_right           = { shift_alt_cmd, "l" },
    swap_up              = { shift_alt_cmd, "k" },
    swap_down            = { shift_alt_cmd, "j" },

    -- position and resize focused window
    center_window        = { alt_cmd, "c" },
    full_width           = { alt_cmd, "f" },
    cycle_width          = { alt_cmd, "r" },
    reverse_cycle_width  = { shift_alt_cmd, "r" },
    cycle_height         = { shift_alt_cmd, "r" },
    reverse_cycle_height = { shift_hyper, "r" },

    -- move focused window into / out of a column
    slurp_in             = { alt_cmd, "i" },
    barf_out             = { alt_cmd, "o" },
})
PaperWM.window_filter:rejectApp("Katja")
PaperWM.window_filter:rejectApp("Screens 5")
PaperWM.window_filter:rejectApp("Strongbox")
PaperWM.window_filter:rejectApp("Finder")
PaperWM.window_filter:rejectApp("Fantastical Helper")
PaperWM.window_ratios = { 0.33, 0.50, 0.66 }
-- number of fingers to detect a horizontal swipe, set to 0 to disable
PaperWM.swipe_fingers = 3
-- increase this number to make windows move futher when swiping
PaperWM.swipe_gain = 1
PaperWM:start() -- restart for new window filter to take effect
