# dotfiles repo

These dotfiles are managed by chezmoi

## Installation

You can install and apply these dotfiles in a single command:

```bash
sh -c "$(curl -fsLS get.chezmoi.io)" -- init --apply https://git.chriswb.dev/chrisw-b/dotfiles
```